package com.fetech.melon.idfactory.increment;


import java.util.Set;

/**
 * Created by ZhangGang on 2017/9/29.
 */
public interface IncrementLongService {


    void init(String key, long start);

    Long getNewId(String key);

    Set<String> getKeys();

}
